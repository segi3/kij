#!/bin/bash

enc=0
dec=0
text=0
filein=""
fileout=""

while getopts p:i:o:edht flag
do
    case "${flag}" in
        p) passphrase=${OPTARG};;
        i) filein=${OPTARG};;
        o) fileout=${OPTARG};;
        t) text=1;;
        e) enc=1;;
        d) dec=1;;
    esac
done

echo "enc: $enc";
echo "dec: $dec";

if [[ "$enc" == "$dec" ]]
then
    echo "ERR: enc dan dec sama";
    exit 1;
fi 

if [[ "$enc" == "1" ]]
    then
        if [ -z ${passphrase} ];
            then
                echo "encrypting file with unset pass..";
                openssl aes-256-cbc -a -salt -in "$filein" -out "$fileout"
            else 
                echo "encrypting file with pass [$passphrase]..";
                openssl aes-256-cbc -a -salt -in "$filein" -out "$fileout" -pass pass:"$passphrase"
        fi
fi

if [[ "$dec" == "1" ]]
    then
        echo "decrypting.."
        if [ -z ${passphrase} ];
            then
                echo "decrypting file with unset pass..";
                openssl aes-256-cbc -d -a -in "$filein" -out "$fileout"
            else 
                echo "decrypting file with pass [$passphrase]..";
                openssl aes-256-cbc -d -a -in "$filein" -out "$fileout" -pass pass:"$passphrase"
        fi
fi

# if [ -z ${passphrase} ];
#     then
#         echo "passphrase is unset";
#     else 
#         echo "passphrase: $passphrase";
# fi