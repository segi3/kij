## Script untuk encrypt/decrypt menggunakan AES

Flag untuk encrypt
- `-e` flag untuk encrypt
- `-i` file yang akan di encrypt
- `-o` file hasil encrypt
- `-p` Set passphrase encrypt

contoh penggunaan (pastikan sudah menjadi executable dengan `chmod +x aes.sh`)

```bash
./aes.sh -e -i file.txt -o file.txt.enc -p kijasik
```

Flag untuk decrypt
- `-e` flag untuk decrypt
- `-i` file yang akan di decrypt
- `-o` file hasil decrypt
- `-p` Set passphrase decrypt

contoh penggunaan

```bash
./aes.sh -d -i file.txt.enc -o file.txt.new -p kijasik
```

### openssl

pake file

openssl aes-256-cbc -a -salt -in file.txt -out file.txt.enc -pass pass:test

openssl aes-256-cbc -d -a -in file.txt.enc -out file.txt.new -pass pass:test

pake text

echo 'hello world' | openssl aes-256-cbc -a -salt -out test.txt.enc -pass pass:test

openssl aes-256-cbc -d -a -in test.txt.enc -pass pass:test

### Ref

https://cyberoast.com/how-to-encrypt-and-decrypt-with-symmetric-ciphers/

https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script

https://tombuntu.com/index.php/2007/12/12/simple-file-encryption-with-openssl/

