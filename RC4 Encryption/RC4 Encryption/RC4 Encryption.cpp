// RC4 Encryption.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string.h>

void swap(unsigned char* a, unsigned char* b);

int main() {
    const char* key = "thisismykey";
    int keyLength = strlen(key);
    char message[256];
    std::cin.getline(message, 256);
    int messageLength = strlen(message);
    std::cout << messageLength << std::endl;
    //generate all possible bytes
    unsigned char s[256];
    for (int i = 0; i < 256; i++) {
        s[i] = i;
    }
    int j = 0;
    for (int i = 0; i < 256; i++) {
        j = (j + s[i] + key[i % keyLength]) % 256;
        swap(&s[i], &s[j]);
    }
    int i = 0;
    j = 0;
    for (int currentChar = 0; currentChar < messageLength; currentChar++) {
        i = (i + 1) % 256;
        j = (j + s[i]) % 256;
        swap(&s[i], &s[j]);
        std::cout << (char)(s[(s[i] + s[j]) % 256] ^ message[currentChar]);
    }
    std::cout << std::endl;
    //std::cout << "Hello World!\n";
}

void swap(unsigned char * a, unsigned char* b) {
    char temp = *a;
    *a = *b;
    *b = temp;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
