### KIJ AES Encryption Analysis

## Application

The chat application is made in python that is avaiable in both GUI and CLI. Before running the application, make sure to install the required python libraries in requirements.txt.

```bash
pip install -r requirements.txt
```

and run the server

```bash
python server_thread_chat.py
```

To run the GUI version, run:

```bash
python chat-ui.py
```

To run the CLI version, run:

```bash
python chat_cli.py {username} {password}
```

Available users to use:
- username: messi, password: surabaya
- username: henderson, password: surabaya
- username lineker, password: surabaya

Besides private chat room, there also exists chat room for groups of users, there is one available group named group1 consisting of messi, henderson, and lineker.
